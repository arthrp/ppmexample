﻿// See https://aka.ms/new-console-template for more information

namespace PpmWriteExample;

public static class Program
{
    public static void Main(string[] args)
    {
        WritePpm("hello1.ppm",500,500);
    }

    private static void WritePpm(string fileName, int width, int height)
    {
        using (var writer = new StreamWriter(fileName))
        {
            writer.WriteLine("P6");
            writer.WriteLine($"{width} {height}");
            writer.WriteLine("255");
            writer.Close();
        }

        using (var binaryWriter = new FileStream(fileName, FileMode.Append))
        {
            var data = GetBinaryData(width, height);
            
            binaryWriter.Write(data);
            binaryWriter.Close();
        }
    }

    private static byte[] GetBinaryData(int width, int height)
    {
        IEnumerable<byte> result = new List<byte>();
        var b = new[] {(byte) 0, (byte) 0, (byte) 255};
        
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                result = result.Concat(b);
            }
        }

        return result.ToArray();
    }
}